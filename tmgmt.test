<?php

/*
 * @file
 * Contains tests for Translation management
 */

/**
 * Base class for tests.
 */
class TMGMTBaseTestCase extends DrupalWebTestCase {
  protected $profile = 'testing';

  /**
   * A default translator using the test translator.
   *
   * @var TMGMTTranslator
   */
  protected $default_translator;

  /**
   * Overrides DrupalWebTestCase::setUp()
   */
  function setUp(array $modules = array()) {
    $modules = array_merge(array('entity', 'tmgmt', 'tmgmt_test'), $modules);
    parent::setUp($modules);
    $this->default_translator = tmgmt_translator_load('test_translator');
    $this->admin_user = $this->drupalCreateUser(array('administer languages', 'access administration pages', 'administer content types', 'administer tmgmt'));
  }

  /**
   * Creates, saves and returns a translator.
   *
   * @return TMGMTTranslator
   */
  function createTranslator() {
    $translator = new TMGMTTranslator();
    $translator->name = strtolower($this->randomName());
    $translator->label = $this->randomName();
    $translator->plugin = 'test_translator';
    $translator->settings = array(
      'key' => $this->randomName(),
      'another_key' => $this->randomName(),
    );
    $this->assertEqual(SAVED_NEW, $translator->save());

    // Assert that the translator was assigned a tid.
    $this->assertTrue($translator->tid > 0);
    return $translator;
  }

  /**
   * Creates, saves and returns a translation job.
   *
   * @return TMGMTJob
   */
  function createJob($source = 'en', $target = 'de', $uid = 1)  {
    $job = tmgmt_job_create($source, $target, $uid);
    $this->assertEqual(SAVED_NEW, $job->save());

    // Assert that the translator was assigned a tid.
    $this->assertTrue($job->tjid > 0);
    return $job;
  }


  /**
   * Sets the proper environment.
   *
   * Currently just adds a new language.
   *
   * @param string $langcode
   *   The language code.
   */
  function setEnvironment($langcode) {
    // Add the language.
    $edit = array(
      'langcode' => $langcode,
    );
    $this->drupalPost('admin/config/regional/language/add', $edit, t('Add language'));
    $this->assertText($langcode, t('Language added successfully.'));
    // This is needed to refresh the static cache of the language list.
    $languages = &drupal_static('language_list');
    $languages = NULL;
  }

}

/**
 * Basic CRUD tests.
 */
class TMGMTCRUDTestCase extends TMGMTBaseTestCase {

  /**
   * Implements getInfo().
   */
  static function getInfo() {
    return array(
      'name' => t('CRUD tests'),
      'description' => t('Basic crud operations for jobs and translators'),
      'group' => t('Translation Management'),
    );
  }

  /**
   * Test crud operations of translators.
   */
  function testTranslators() {
    $translator = $this->createTranslator();

    $loaded_translator = tmgmt_translator_load($translator->tid);

    $this->assertEqual($translator->name, $loaded_translator->name);
    $this->assertEqual($translator->label, $loaded_translator->label);
    $this->assertEqual($translator->settings, $loaded_translator->settings);

    // Update the settings.
    $translator->settings['new_key'] = $this->randomString();
    $this->assertEqual(SAVED_UPDATED, $translator->save());

    $loaded_translator = tmgmt_translator_load($translator->tid);

    $this->assertEqual($translator->name, $loaded_translator->name);
    $this->assertEqual($translator->label, $loaded_translator->label);
    $this->assertEqual($translator->settings, $loaded_translator->settings);

    // Delete the translator, make sure the translator is gone.
    $translator->delete();
    $this->assertFalse(tmgmt_translator_load($translator->tid));
  }

  /**
   * Test crud operations of jobs.
   */
  function testJobs() {
    $job = $this->createJob();

    $loaded_job = tmgmt_job_load($job->tjid);

    $this->assertEqual($job->source_language, $loaded_job->source_language);
    $this->assertEqual($job->target_language, $loaded_job->target_language);

    // Assert that the created and changed information has been set to the
    // default value.
    $this->assertTrue($loaded_job->created > 0);
    $this->assertTrue($loaded_job->changed > 0);
    $this->assertEqual(0, $loaded_job->state);

    // Update the settings.
    $job->reference = 7;
    $this->assertEqual(SAVED_UPDATED, $job->save());

    $loaded_job = tmgmt_job_load($job->tjid);

    $this->assertEqual($job->reference, $loaded_job->reference);

    // Test the job items.
    $item1 = $job->addItem('plugin', 'type', 5);
    $item2 = $job->addItem('plugin', 'type', 4);

    // Load and compare the items.
    $items = $job->getItems();
    $this->assertEqual(2, count($items));

    $this->assertEqual($item1->plugin, $items[$item1->tjiid]->plugin);
    $this->assertEqual($item1->item_type, $items[$item1->tjiid]->item_type);
    $this->assertEqual($item1->item_id, $items[$item1->tjiid]->item_id);
    $this->assertEqual($item2->plugin, $items[$item2->tjiid]->plugin);
    $this->assertEqual($item2->item_type, $items[$item2->tjiid]->item_type);
    $this->assertEqual($item2->item_id, $items[$item2->tjiid]->item_id);

    // Delete the translator, make sure the translator is gone.
    $job->delete();
    $this->assertFalse(tmgmt_job_load($job->tjid));
  }

  /**
   * Test crud operations of job items.
   */
  function testJobItems() {
    $job = $this->createJob();

    // Add some test items.
    $item1 = $job->addItem('plugin', 'type', 5);
    $item2 = $job->addItem('plugin', 'type', 4);

    // Test single load callback.
    $item = tmgmt_job_item_load($item1->tjiid);
    $this->assertEqual($item1->plugin, $item->plugin);
    $this->assertEqual($item1->item_type, $item->item_type);
    $this->assertEqual($item1->item_id, $item->item_id);

    // Test multiple load callback.
    $items = tmgmt_job_item_load_multiple(array($item1->tjiid, $item2->tjiid));

    $this->assertEqual(2, count($items));

    $this->assertEqual($item1->plugin, $items[$item1->tjiid]->plugin);
    $this->assertEqual($item1->item_type, $items[$item1->tjiid]->item_type);
    $this->assertEqual($item1->item_id, $items[$item1->tjiid]->item_id);
    $this->assertEqual($item2->plugin, $items[$item2->tjiid]->plugin);
    $this->assertEqual($item2->item_type, $items[$item2->tjiid]->item_type);
    $this->assertEqual($item2->item_id, $items[$item2->tjiid]->item_id);
  }
}

/**
 * Tests interaction between core and the plugins.
 */
class TMGMTPluginsTestCase extends TMGMTBaseTestCase {

  /**
   * Implements getInfo().
   */
  static function getInfo() {
    return array(
      'name' => t('Plugin tests'),
      'description' => t('Verifies basic functionality of source and translator plugins'),
      'group' => t('Translation Management'),
    );
  }

  function createJob($action = 'translate') {
    $job = parent::createJob();

    for ($i = 1; $i < 3; $i++) {
      if ($i == 3) {
        // Explicitly define the data for the third item.
        $data['data'] = array(
          'dummy' => array(
            'deep_nesting' => array(
              '#text' => 'Stored data',
            ),
          ),
        );
        $job->addItem('test_source', 'test', $i, array($data));
      }
      $job->addItem('test_source', 'test', $i);
    }

    // Manually specify the translator for now.
    $job->translator = $this->default_translator->name;
    $job->settings = array('action' => $action);

    return $job;
  }

  function testBasicWorkflow() {

    // Submit a translation job.
    $submit_job = $this->createJob('submit');
    $submit_job->requestTranslation();
    $submit_job = tmgmt_job_load($submit_job->tjid);
    $this->assertTrue($submit_job->isActive());
    $messages = $submit_job->getMessages();
    $last_message = end($messages);
    $this->assertEqual('Test submit.', $last_message->message);

    // Translate a job.
    $translate_job = $this->createJob('translate');
    $translate_job->requestTranslation();
    $translate_job = tmgmt_job_load($translate_job->tjid);
    foreach ($translate_job->getItems() as $job_item) {
      $this->assertTrue($job_item->isNeedsReview());
    }

    $messages = $translate_job->getMessages();
    // array_values() results in numeric keys, which is necessary for list.
    list($debug, $translated, $needs_review) = array_values($messages);
    $this->assertEqual('Test translator called.', $debug->message);
    $this->assertEqual('debug', $debug->type);
    $this->assertEqual('Test translation created.', $translated->message);
    $this->assertEqual('status', $translated->type);

    // The third message is specific to a job item and has different state
    // constants.
    $this->assertEqual('The translation for !source is finished and can now be reviewed.', $needs_review->message);
    $this->assertEqual('status', $needs_review->type);

    $i = 1;
    foreach ($translate_job->getItems() as $item) {
      // Check the translated text.
      if ($i != 3) {
        $expected_text = 'de_Text for job item with type ' . $item->item_type . ' and id ' . $item->item_id . '.';
      }
      else {
        // The third item has an explicitly stored data value.
        $expected_text = 'de_Stored data';
      }
      $item_data = $item->getData();
      $this->assertEqual($expected_text, $item_data['dummy']['deep_nesting']['#translation']['#text']);
      $i++;
    }

    foreach ($translate_job->getItems() as $job_item) {
      $job_item->acceptTranslation();
    }

    // @todo Accepting does not result in messages on the job anymore.
    // Update once there are job item messages.
    /*
    $messages = $translate_job->getMessages();
    $last_message = end($messages);
    $this->assertEqual('Job accepted', $last_message->message);
    $this->assertEqual('status', $last_message->type);*/

    // Check if the translations have been "saved".
    foreach ($translate_job->getItems() as $item) {
      $this->assertTrue(variable_get('tmgmt_test_saved_translation_' . $item->item_type . '_' . $item->item_id, FALSE));
    }

    // A rejected job.
    $reject_job = $this->createJob('reject');
    $reject_job->requestTranslation();
    // Still rejected.
    $this->assertTrue($reject_job->isRejected());

    $messages = $reject_job->getMessages();
    $last_message = end($messages);
    $this->assertEqual('This is not supported.', $last_message->message);
    $this->assertEqual('error', $last_message->type);

    // A failing job.
    $failing_job = $this->createJob('fail');
    $failing_job->requestTranslation();
    // Still new.
    $this->assertTrue($failing_job->isUnprocessed());

    $messages = $failing_job->getMessages();
    $last_message = end($messages);
    $this->assertEqual('Service not reachable.', $last_message->message);
    $this->assertEqual('error', $last_message->type);
  }
}


/**
 * Test the helper functions in tmgmt.module.
 */
class TMGMTHelperTestCase extends TMGMTBaseTestCase {

  /**
   * Implements getInfo().
   */
  static function getInfo() {
    return array(
      'name' => t('Helper functions Test case'),
      'description' => t('Helper functions for other modules'),
      'group' => t('Translation Management'),
    );
  }

  /**
   * Tests tmgmt_job_match_item()
   *
   * @see tmgmt_job_match_item
   */
  function testTMGTJobMatchItem() {
    $this->drupalLogin($this->admin_user);
    $this->setEnvironment('fr');
    $this->setEnvironment('es');

    // Add a job from en to fr and en to sp.
    $job_en_fr = $this->createJob('en', 'fr');
    $job_en_sp = $this->createJob('en', 'es');

    // Add a job which has existing source-target combinations.
    $this->assertEqual($job_en_fr->tjid, tmgmt_job_match_item('en', 'fr')->tjid);
    $this->assertEqual($job_en_sp->tjid, tmgmt_job_match_item('en', 'es')->tjid);

    // Add a job which has no existing source-target combination.
    $this->assertTrue(tmgmt_job_match_item('fr', 'es'));
  }

}
