<?php

/**
 * Basic Entity Source tests.
 *
 * @todo: Write tests for these cases:
 *  - any generic entity (defined by the tmgmt_entity_test module).
 */
class TMGMTEntitySourceTestCase extends TMGMTBaseTestCase {

  /**
   * Implements getInfo().
   */
  static function getInfo() {
    return array(
      'name' => t('Entity Source tests'),
      'description' => t('Exporting source data from entities and saving translations back to entities.'),
      'group' => t('Translation Management'),
      'dependencies' => array('waiting_for_new_release_entity_translation'),
    );
  }

  /**
   * Overrides SimplenewsTestCase::setUp()
   */
  function setUp() {
    parent::setUp(array('tmgmt_entity', 'tmgmt_entity_test', 'taxonomy', 'entity_translation'));
    $this->admin_user = $this->drupalCreateUser(array('administer languages', 'access administration pages', 'administer content types', 'administer taxonomy'));

    // Create a simple vocabulary.
    $this->vocabulary = new stdClass();
    $this->vocabulary->name = $this->randomName();
    $this->vocabulary->machine_name = strtolower($this->randomName());
    taxonomy_vocabulary_save($this->vocabulary);
  }

  /**
   * Adds some fields to an entity bundle
   *
   * @param $entity_type
   *  The entity type.
   * @param $bundle
   *  The entity bundle.
   */
  function addFields($entity_type, $bundle) {
    $translatables = array(FALSE, TRUE, TRUE, TRUE);
    $field_types = array('text', 'text_with_summary');

    foreach ($translatables as $switch) {
      $field_type = $field_types[array_rand($field_types, 1)];
      $field_name = drupal_strtolower($this->randomName());

      // Create a field.
      $field = array(
        'field_name' => $field_name,
        'type' => $field_type,
        'cardinality' => mt_rand(1, 5),
        'translatable' => $switch,
      );
      field_create_field($field);

      // Create an instance of the previously created field.
      $instance = array(
        'field_name' => $field_name,
        'entity_type' => $entity_type,
        'bundle' => $bundle,
        'label' => $this->randomString(10),
        'description' => $this->randomString(30),
        'widget' => array(
          'type' => $field_type == 'text' ? 'text_textfield' : 'text_textarea_with_summary',
          'label' => $this->randomString(10),
        ),
      );
      field_create_instance($instance);

      $this->field_names[$entity_type][$bundle][] = $field_name;
    }
  }

  /**
   * Tests nodes field translation.
   */
  function testEntitySourceNode() {
    $this->drupalLogin($this->admin_user);
    $this->setEnvironment('de');

    // Add some fields.
    $this->addFields('node', 'article');

    // Create a translation job.
    $job = $this->createJob();
    $job->translator = $this->default_translator->name;
    $job->settings = array();
    $job->save();

    // Create some nodes.
    for ($i = 1; $i <= 5; $i++) {
      $node = array(
        'type' => 'article',
        'language' => 'en',
        'body' => array('en' => array(array())),
      );

      // Put some values in the new fields.
      foreach ($this->field_names['node']['article'] as $field_name) {
        $field_info = field_info_field($field_name);
        $field_lang = $field_info['translatable'] ? 'en' : LANGUAGE_NONE;
        $cardinality = $field_info['cardinality'] == FIELD_CARDINALITY_UNLIMITED ? 1 : $field_info['cardinality'];

        // Create two deltas for each field.
        for ($delta = 0; $delta <= $cardinality; $delta++) {
          $node[$field_name][$field_lang][$delta]['value'] = $this->randomString(20);
          if ($field_info['type'] == 'text_with_summary') {
            $node[$field_name][$field_lang][$delta]['summary'] = $this->randomString(10);
          }
        }
      }

      $node = $this->drupalCreateNode($node);

      // Create a job item for this node and add it to the job.
      $job->addItem('entity', 'node', $node->nid);
    }

    // Translate the job.
    $job->requestTranslation();

    // Check the translated job items.
    foreach ($job->getItems() as $item) {
      $item->acceptTranslation();
      $this->assertTrue($item->isAccepted());
      $entity = entity_load_single($item->item_type, $item->item_id);
      $data = $item->getData();
      $this->checkTranslatedData($entity, $data, 'de');
      $this->checkUntranslatedData($entity, $this->field_names['node']['article'], $data, 'de');
    }
  }

  /**
   * Tests taxonomy terms field translation.
   */
  function testEntitySourceTerm() {
    $this->drupalLogin($this->admin_user);
    $this->setEnvironment('de');

    // Add some fields.
    $this->addFields('taxonomy_term', 'tags');

    // Create the job.
    $job = $this->createJob();
    $job->translator = $this->default_translator->name;
    $job->settings = array();
    $job->save();

    //Create some terms.
    for ($i = 1; $i <= 5; $i++) {
      $term = new stdClass();
      $term->name = $this->randomName();
      $term->description = $this->randomName();
      $term->vid = $this->vocabulary->vid;
      // Put some values in the new fields.
      foreach ($this->field_names['taxonomy_term']['tags'] as $field_name) {
        $field_info = field_info_field($field_name);
        $field_lang = $field_info['translatable'] ? 'en' : LANGUAGE_NONE;
        $cardinality = $field_info['cardinality'] == FIELD_CARDINALITY_UNLIMITED ? 1 : $field_info['cardinality'];

        for ($delta = 0; $delta <= $cardinality; $delta++) {
          $term->{$field_name}[$field_lang][$delta]['value'] = $this->randomString(20);
          if ($field_info['type'] == 'text_with_summary') {
            $term->{$field_name}[$field_lang][$delta]['summary'] = $this->randomString(10);
          }
        }
      }
      taxonomy_term_save($term);
      // Create the item and assign it to the job.
      $job->addItem('entity', 'taxonomy_term', $term->tid);
    }
    // Request the translation and accept it.
    $job->requestTranslation();

    // Check if the fields were translated.
    foreach ($job->getItems() as $item) {
      $item->acceptTranslation();
      $entity = entity_load_single($item->item_type, $item->item_id);
      $data = $item->getData();
      $this->checkTranslatedData($entity, $data, 'de');
      $this->checkUntranslatedData($entity, $this->field_names['taxonomy_term']['tags'], $data, 'de');
    }
  }

  /**
   * Compares the data from an entity with the translated data.
   *
   * @param $tentity
   *  The translated entity object.
   * @param $data
   *  An array with the translated data.
   * @param $langcode
   *  The code of the target language.
   */
  function checkTranslatedData($tentity, $data, $langcode) {
    foreach (element_children($data) as $field_name) {
      foreach (element_children($data[$field_name]) as $delta) {
        foreach (element_children($data[$field_name][$delta]) as $column) {
          $column_value = $data[$field_name][$delta][$column];
          $this->assertEqual($tentity->{$field_name}[$langcode][$delta][$column], $column_value['#translation']['#text'], t('The field %field:%delta has been populated with the proper translated data.', array('%field' => $field_name, 'delta' => $delta)));
        }
      }
    }
  }

  /**
   * Checks the fields that should not be translated.
   *
   * @param $tentity
   *  The translated entity object.
   * @param $fields
   *  An array with the field names to check.
   * @param $translation
   *  An array with the translated data.
   * @param $langcode
   *  The code of the target language.
   */
  function checkUntranslatedData($tentity, $fields, $data, $langcode) {
    foreach ($fields as $field_name) {
      $field_info = field_info_field($field_name);
      if (!$field_info['translatable']) {
        // Avoid some PHP warnings.
        if (isset($data[$field_name])) {
          $this->assertNull($data[$field_name]['#translation']['#text'], t('The not translatable field was not translated.'));
        }
        if (isset($tentity->{$field_name}[$langcode])) {
          $this->assertNull($tentity->{$field_name}[$langcode], t('The entity has translated data in a field that is translatable.'));
        }
      }
    }
  }
}
